# @html-validate/semantic-release-monorepo changelog

## 6.8.1 (2025-03-09)

### Bug Fixes

- **deps:** update dependency lerna to v8.2.1 4d3c621

## 6.8.0 (2025-02-23)

### Features

- **deps:** update dependency @semantic-release/exec to v7 e7471b3

### Bug Fixes

- **deps:** update dependency lerna to v8.2.0 805508c
- **deps:** update dependency semantic-release to v24.2.3 5b3de82
- **deps:** update dependency semantic-release-lerna to v2.11.1 bab42d4

## 6.7.11 (2025-02-16)

### Bug Fixes

- **deps:** update dependency semantic-release to v24.2.2 ad97594

## 6.7.10 (2025-02-09)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.11.0 5cd6bde

## 6.7.9 (2025-01-19)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.2.4 9329662

## 6.7.8 (2025-01-05)

### Bug Fixes

- **deps:** update semantic-release monorepo b64e8d9

## 6.7.7 (2024-12-29)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.10.0 0b93fad

## 6.7.6 (2024-12-22)

### Bug Fixes

- **deps:** update dependency @semantic-release/release-notes-generator to v14.0.2 206b733

## 6.7.5 (2024-12-08)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.2.2 b305ab7
- **deps:** update dependency @semantic-release/gitlab to v13.2.3 648807b

## 6.7.4 (2024-11-03)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.9 b908947

## 6.7.3 (2024-10-27)

### Bug Fixes

- **deps:** update dependency semantic-release to v24.2.0 bbb5a0b

## 6.7.2 (2024-10-20)

### Bug Fixes

- **deps:** update dependency semantic-release to v24.1.3 09da36c

## 6.7.1 (2024-10-13)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.9.0 1f63603

## 6.7.0 (2024-09-29)

### Features

- **deps:** remove `find-up` dependency 51542b2
- **deps:** require node v20 for release-scripts e6a623d

### Bug Fixes

- **deps:** update dependency semantic-release to v24.1.2 7c1a066

## 6.6.2 (2024-09-15)

### Bug Fixes

- **deps:** update dependency semantic-release to v24.1.1 52142c9

## 6.6.1 (2024-09-09)

### Bug Fixes

- **@html-validate/semantic-release-config:** publish release candidates from next e6d9832
- **@html-validate/semantic-release-monorepo-config:** use same branch configuration on monorepo layout be24a29

## 6.6.0 (2024-09-08)

### Features

- **@html-validate/semantic-release-config:** enable releases on `next` branch 4210b34

## 6.5.8 (2024-08-25)

### Bug Fixes

- **deps:** update dependency semantic-release to v24.1.0 e642772

## 6.5.7 (2024-08-18)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.8.0 74d3558

## 6.5.6 (2024-08-11)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.8 4002adc

## 6.5.5 (2024-08-04)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.2.1 3db541d

## 6.5.4 (2024-07-28)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.7 c30180a

## 6.5.3 (2024-07-14)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.2.0 6bea587

## 6.5.2 (2024-07-07)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.6 3331d9f

## 6.5.1 (2024-06-30)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.4 d3a6e59
- **deps:** update dependency lerna to v8.1.5 d08fa0f

## 6.5.0 (2024-06-23)

### Features

- **deps:** update dependency conventional-changelog-conventionalcommits to v8 8624a59
- **deps:** update semantic-release monorepo a27cd2b

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.7.0 9814d64

## 6.4.4 (2024-05-19)

### Bug Fixes

- **deps:** update dependency lerna to v8.1.3 f1e3079

## 6.4.3 (2024-05-12)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.1.0 e7c582f
- **deps:** update dependency @semantic-release/npm to v12.0.1 af99346
- **deps:** update dependency semantic-release to v23.1.1 0442d0c

## 6.4.2 (2024-05-05)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.0.4 2e4a43a
- **deps:** update dependency semantic-release-lerna to v2.6.0 d247e5e
- **deps:** update dependency semantic-release-lerna to v2.6.1 3ab938b
- **deps:** update dependency semantic-release-lerna to v2.6.2 5665ab6

## 6.4.1 (2024-04-14)

### Bug Fixes

- **deps:** update dependency semantic-release to v23.0.8 c9eb7ca

## 6.4.0 (2024-04-07)

### Features

- **deps:** update dependency @semantic-release/release-notes-generator to v13 48a2c68

### Bug Fixes

- **deps:** update dependency semantic-release to v23.0.7 ad331b4

## 6.3.2 (2024-03-31)

### Bug Fixes

- **deps:** update dependency semantic-release to v23.0.6 5129ed7

## 6.3.1 (2024-03-24)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.5.0 1fabd1a

## 6.3.0 (2024-03-20)

### Features

- **deps:** update dependency @semantic-release/npm to v12 d65a59a

### Bug Fixes

- **deps:** update dependency semantic-release to v23.0.4 8c711e4
- **deps:** update dependency semantic-release to v23.0.5 f292ba5
- **deps:** update semantic-release-lerna to v2.4.0 60f696e

## 6.2.4 (2024-03-17)

### Bug Fixes

- **deps:** update dependency semantic-release to v23.0.3 5c7a41c

## 6.2.3 (2024-03-10)

### Bug Fixes

- **deps:** update dependency @semantic-release/npm to v11.0.3 87e3bd5

## 6.2.2 (2024-2-25)

### Bug Fixes

- **@html-validate/semantic-release-config:** try to fix broken linkCompare option b313f5a

## 6.2.1 (2024-2-25)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.0.3 5a65a83
- **deps:** update dependency semantic-release-lerna to v2.3.0 5c249de

## 6.2.0 (2024-2-11)

### Features

- **deps:** support semantic-release v23 637040d

### Bug Fixes

- **deps:** update dependency lerna to v8.1.2 2576281

## 6.1.2 (2024-2-4)

### Bug Fixes

- **deps:** update dependency semantic-release-lerna to v2.2.0 facc7e6

## 6.1.1 (2024-1-14)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13.0.2 14d4116

## 6.1.0 (2024-1-8)

### Features

- **@html-validate/semantic-release-config:** disable compare link in changelog 7e8c3af

### Bug Fixes

- **deps:** update dependency lerna to v8.0.2 d5df350

## 6.0.1 (2023-12-31)

### Bug Fixes

- **@html-validate/release-scripts:** strip volta field from package.json 65cb5b4
- **deps:** update dependency lerna to v8 0bd0f5b

## 6.0.0 (2023-12-28)

### ⚠ BREAKING CHANGES

- **deps:** minimum version of NodeJS is now v20 (except for
  `@html-validate/release-scripts` which still supports v16).

### Features

- **deps:** require nodejs 20 74aa135

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v13 f641de2

## 5.0.5 (2023-12-17)

### Bug Fixes

- **deps:** update dependency semantic-release to v22.0.11 d6208ff
- **deps:** update dependency semantic-release to v22.0.12 eef141f
- **deps:** update dependency semantic-release-lerna to v2.1.0 1b5dd22

## 5.0.4 (2023-12-10)

### Bug Fixes

- **@html-validate/release-scripts:** strip overrides field ca030f6
- **deps:** update dependency @semantic-release/npm to v11.0.2 d9f52db
- **deps:** update dependency semantic-release to v22.0.10 399957d
- **deps:** update dependency semantic-release to v22.0.9 a9b2b72

## 5.0.3 (2023-11-26)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v12.1.0 af5e2ee
- **deps:** update dependency @semantic-release/gitlab to v12.1.1 c637070
- **deps:** update dependency semantic-release to v22.0.8 3605c8e

## 5.0.2 (2023-11-12)

### Bug Fixes

- **deps:** update dependency @semantic-release/release-notes-generator to v12.1.0 e4f0cc5

## 5.0.1 (2023-11-05)

### Bug Fixes

- **deps:** update dependency @semantic-release/npm to v11.0.1 71f66c8
- **deps:** update dependency @semantic-release/release-notes-generator to v12.0.1 7df20be
- **deps:** update dependency lerna to v7.4.2 2c3dc35
- **deps:** update dependency semantic-release to v22.0.6 ad32df9
- **deps:** update dependency semantic-release to v22.0.7 a020975

## 5.0.0 (2023-10-22)

### ⚠ BREAKING CHANGES

- **deps:** minimum version of semantic-release is now v22

### Features

- **deps:** support semantic-release v22 ee2f8d0

### Bug Fixes

- **deps:** update dependency conventional-changelog-conventionalcommits to v7 fd8215d
- **deps:** update dependency lerna to v7.4.0 8a2851d
- **deps:** update dependency lerna to v7.4.1 15081a5

## 4.1.15 (2023-10-15)

### Dependency upgrades

- **deps:** update dependency lerna to v7.3.1 277aa51

## 4.1.14 (2023-09-24)

### Dependency upgrades

- **deps:** update dependency semantic-release to v21.1.2 ceeb6bd

## 4.1.13 (2023-09-17)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v12.0.6 ea5bdf7
- **deps:** update dependency @semantic-release/npm to v10.0.6 c339df7
- **deps:** update dependency lerna to v7.3.0 dea5c4b

## 4.1.12 (2023-09-09)

### Reverts

- **@html-validate/semantic-release-config:** Revert "fix(deps): update dependency conventional-changelog-conventionalcommits to v7" 79ecd1d

## 4.1.11 (2023-09-03)

### Dependency upgrades

- **deps:** update dependency @semantic-release/release-notes-generator to v11.0.5 2ce67eb
- **deps:** update dependency @semantic-release/release-notes-generator to v11.0.6 b229ff4
- **deps:** update dependency @semantic-release/release-notes-generator to v11.0.7 d8661b2
- **deps:** update dependency conventional-changelog-conventionalcommits to v7 2ff1d29
- **deps:** update dependency lerna to v7.2.0 762f237

## 4.1.10 (2023-08-27)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v10.0.5 2f5741b
- **deps:** update dependency semantic-release to v21.0.8 df982fd
- **deps:** update dependency semantic-release to v21.0.9 f6d4bf5
- **deps:** update dependency semantic-release to v21.1.0 02ab371
- **deps:** update dependency semantic-release to v21.1.1 da75bf6

## 4.1.9 (2023-08-20)

### Dependency upgrades

- **deps:** update dependency semantic-release-lerna to v1.0.3 50cace4

## 4.1.8 (2023-08-13)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v12.0.5 fd3585c
- **deps:** update dependency lerna to v7.1.5 fe90a61

## 4.1.7 (2023-08-06)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v12.0.4 fd3bde4

## 4.1.6 (2023-07-30)

### Dependency upgrades

- **deps:** update dependency conventional-changelog-conventionalcommits to v6 f2ea9ca
- **deps:** update dependency lerna to v7 d926497
- **deps:** update dependency semantic-release-lerna to v1.0.2 5e71b2c

## 4.1.5 (2023-07-12)

### Dependency upgrades

- **deps:** support prettier v3 ee067f4

## 4.1.4 (2023-07-09)

### Dependency upgrades

- **deps:** update semantic-release monorepo c4bd4eb

## 4.1.3 (2023-07-02)

### Dependency upgrades

- **deps:** update dependency semantic-release to v21.0.6 af1fc4f

## 4.1.2 (2023-06-11)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v12.0.2 2e1c3c5
- **deps:** update dependency @semantic-release/release-notes-generator to v11.0.3 647ceb0
- **deps:** update dependency conventional-changelog-conventionalcommits to v6 e5f1778
- **deps:** update dependency semantic-release to v21.0.5 2324def
- **deps:** update dependency semantic-release-lerna to v1.0.1 d8332c1
- **deps:** update semantic-release monorepo 06b110e

## 4.1.1 (2023-06-04)

### Dependency upgrades

- **deps:** update dependency @semantic-release/release-notes-generator to v11.0.2 10de439
- **deps:** update dependency semantic-release to v21.0.3 11cde07

## 4.1.0 (2023-05-18)

### Features

- **@html-validate/release-scripts:** support putting externals in `externalDependencies` ddc2541

### Dependency upgrades

- **deps:** update dependency semantic-release-lerna to v1 d9d9a31
- **deps:** update semantic-release monorepo f407d35

## 4.0.7 (2023-05-07)

### Dependency upgrades

- **deps:** update dependency lerna to v6.6.2 8e2ad0b

## 4.0.6 (2023-03-26)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v10 8664981
- **deps:** update dependency lerna to v6.6.0 d23104a
- **deps:** update dependency lerna to v6.6.1 2e1efd3
- **deps:** update semantic-release monorepo 681496c

## 4.0.5 (2023-03-19)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v12 acb9242
- **deps:** update dependency semantic-release to v20.1.3 029fb39
- **deps:** update dependency semantic-release-lerna to v0.8.1 f2442d1

## 4.0.4 (2023-03-05)

### Dependency upgrades

- **deps:** update dependency semantic-release to v20.1.1 8ed31c8

## 4.0.3 (2023-02-19)

### Dependency upgrades

- **deps:** update dependency lerna to v6.5.0 e9c5f18
- **deps:** update dependency lerna to v6.5.1 8b5284e
- **deps:** update dependency semantic-release-lerna to v0.8.0 92968e8

## 4.0.2 (2023-02-12)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v10 ba4d5af

## 4.0.1 (2023-01-29)

### Dependency upgrades

- **deps:** update dependency semantic-release to v20.0.3 79c36b5
- **deps:** update dependency semantic-release to v20.0.4 c243596
- **deps:** update dependency semantic-release to v20.1.0 ff56526

## 4.0.0 (2023-01-22)

### ⚠ BREAKING CHANGES

- **deps:** require NodeJS 18

### Dependency upgrades

- **deps:** update dependency semantic-release to v20 8117d37

## 3.5.22 (2023-01-15)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v9.0.2 4ddfc39
- **deps:** update dependency lerna to v6.4.1 d9fdce3

## 3.5.21 (2023-01-08)

### Dependency upgrades

- **deps:** update dependency lerna to v6.4.0 974b0a8

## 3.5.20 (2023-01-01)

### Dependency upgrades

- **deps:** update dependency lerna to v6.2.0 d03fb99
- **deps:** update dependency lerna to v6.3.0 c9e7670

## 3.5.19 (2022-12-19)

### Bug Fixes

- **@html-validate/semantic-release-config:** use `npm exec` instead of `npm bin` 304e43e

### Dependency upgrades

- **deps:** update dependency semantic-release-lerna to v0.7.2 d887e19

## 3.5.18 (2022-12-11)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.5.1 ca47bee

## 3.5.17 (2022-12-04)

### Dependency upgrades

- **deps:** update dependency @semantic-release/changelog to v6.0.2 9945149
- **deps:** update dependency lerna to v6.1.0 b5e4aea

## 3.5.16 (2022-11-13)

### Dependency upgrades

- **deps:** update dependency lerna to v6.0.3 a9e43b1

## 3.5.15 (2022-11-06)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.5.0 a0fb780

## 3.5.14 (2022-10-23)

### Dependency upgrades

- **deps:** update dependency lerna to v6 6cdf062

## 3.5.13 (2022-10-16)

### Dependency upgrades

- **deps:** update dependency lerna to v5.6.2 afca873
- **deps:** update dependency semantic-release-lerna to v0.7.1 92abfab

## 3.5.12 (2022-10-09)

### Dependency upgrades

- **deps:** update dependency lerna to v5.6.1 c233cfc

## 3.5.11 (2022-10-02)

### Dependency upgrades

- **deps:** update dependency lerna to v5.5.4 7b7f049

## 3.5.10 (2022-09-25)

### Dependency upgrades

- **deps:** update dependency lerna to v5.5.2 99c38f1

## 3.5.9 (2022-09-18)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.4.2 dd00e1f
- **deps:** update dependency lerna to v5.5.1 cb6d339
- **deps:** update dependency semantic-release-lerna to v0.7.0 9f74817

## 3.5.8 (2022-09-04)

### Dependency upgrades

- **deps:** update dependency lerna to v5.5.0 f98a0de

## 3.5.7 (2022-08-28)

### Dependency upgrades

- **deps:** update dependency semantic-release to v19.0.5 013b35e

## 3.5.6 (2022-08-21)

### Dependency upgrades

- **deps:** update dependency lerna to v5.4.1 564f1b8
- **deps:** update dependency lerna to v5.4.2 950e764
- **deps:** update dependency lerna to v5.4.3 34296ed

## 3.5.5 (2022-08-07)

### Bug Fixes

- **@html-validate/release-scripts:** strip `workspaces` field 22e2ff2

### Dependency upgrades

- **deps:** update dependency lerna to v5.3.0 b739915

## 3.5.4 (2022-07-17)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.4.1 97bebfa

## 3.5.3 (2022-07-10)

### Dependency upgrades

- **deps:** update dependency lerna to v5.1.8 a0440b5

## 3.5.2 (2022-06-26)

### Dependency upgrades

- **deps:** update dependency lerna to v5.1.6 6c2c4cd
- **deps:** update dependency semantic-release-lerna to v0.6.1 e4bd5ea

## 3.5.1 (2022-06-17)

### Bug Fixes

- **@html-validate/release-scripts:** default filename for `post{pack,release}` ae815b9

## 3.5.0 (2022-06-16)

### Features

- **@html-validate/release-scripts:** introduce `--retain-scripts` fe3465c
- **@html-validate/release-scripts:** support arguments in any order 90d44d4
- **@html-validate/release-scripts:** use `package.json` as default filename 79a21e6

### Dependency upgrades

- **deps:** update dependency lerna to v5.1.2 fe46949
- **deps:** update dependency lerna to v5.1.4 ec8474f
- **deps:** update dependency semantic-release-lerna to v0.6.0 d664e2c

## 3.4.8 (2022-06-12)

### Dependency upgrades

- **deps:** update dependency lerna to v5.1.0 dec1b76
- **deps:** update dependency lerna to v5.1.1 678a5c0
- **deps:** update dependency semantic-release to v19.0.3 f8c1faf

## 3.4.7 (2022-06-05)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.3.1 a9a66ba
- **deps:** update dependency @semantic-release/gitlab to v9.3.2 7916368

## 3.4.6 (2022-05-28)

### Dependency upgrades

- **deps:** update dependency conventional-changelog-conventionalcommits to v5 64b844a
- **deps:** update dependency semantic-release-lerna to v0.5.0 845e05d

### 3.4.5 (2022-05-25)

### Dependency upgrades

- **deps:** update dependency lerna to v5 0cadd82

### 3.4.4 (2022-05-22)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.3.0 04672eb

### 3.4.3 (2022-05-15)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.2.1 5a1c4c9

### 3.4.2 (2022-05-01)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.2.0 8cccdb2

### 3.4.1 (2022-04-29)

### Bug Fixes

- **@html-validate/release-scripts:** fix swapped script names 66588bc

## 3.4.0 (2022-04-22)

### Features

- **@html-validate/release-scripts:** add release-{pre,post}publish 99c57d2

### 3.3.4 (2022-04-17)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9.1.0 197f7db
- **deps:** update dependency @semantic-release/gitlab to v9.1.1 fa5c29f
- **deps:** update dependency @semantic-release/gitlab to v9.1.2 9ab2aef

### 3.3.3 (2022-04-10)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v9 29cbe2d

### 3.3.2 (2022-04-10)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v8.1.0 12ca224

### 3.3.1 (2022-04-03)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v8.0.2 439667b

## 3.3.0 (2022-03-24)

### Features

- **@html-validate/release-scripts:** remove `husky`, `simple-git-hooks` and `stylelint` during release b303ba2
- **@html-validate/release-scripts:** rewrite {pre,post}pack scripts to js bc128b7

### Bug Fixes

- **@html-validate/release-scripts:** remove more fields 69f0067

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v7.2.1 dcf950c
- **deps:** update dependency @semantic-release/gitlab to v8 7de6207

### 3.2.4 (2022-03-20)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v7.2.0 3260f79

### 3.2.3 (2022-03-20)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v7.1.0 78c4057

### 3.2.2 (2022-03-06)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v9.0.1 4dcdf44

### 3.2.1 (2022-02-26)

### Bug Fixes

- **@html-validate/release-scripts:** resolve path to js library 5716088

## 3.2.0 (2022-02-26)

### Features

- **@html-validate/release-scripts:** new flag `--external` 33a47e5

## 3.1.0 (2022-02-20)

### Features

- **@html-validate/release-scripts:** adding release scripts a1eca5b

### 3.0.9 (2022-02-15)

### Bug Fixes

- **@html-validate/semantic-release-monorepo-config:** let preset carry lerna as dependency 931bb6d

### 3.0.8 (2022-02-15)

### Bug Fixes

- **@html-validate/semantic-release-monorepo-config:** commit all package.json from all workspaces 2ac0874

### 3.0.7 (2022-02-15)

### Bug Fixes

- **@html-validate/semantic-release-monorepo-config:** dont commit lerna.json when releasing cd981f1

### 3.0.6 (2022-01-20)

### Bug Fixes

- **@html-validate/semantic-release-config:** proper semantic-release dependency e9be35c

### 3.0.5 (2022-01-20)

### Bug Fixes

- **@html-validate/semantic-release-config:** wrap semantic-release binary 90d62b6

### Dependency upgrades

- **deps:** update semantic-release monorepo b3f30fe

### 3.0.4 (2022-01-06)

### Dependency upgrades

- **deps:** update dependency @semantic-release/exec to v6.0.3 e0d2c29
- **deps:** update dependency conventional-changelog-conventionalcommits to v4.6.3 5a95945

### 3.0.3 (2021-12-05)

### Dependency upgrades

- **deps:** update semantic-release monorepo cb86c22

### 3.0.2 (2021-11-13)

### Dependency upgrades

- **deps:** update semantic-release monorepo 60f3931

### 3.0.1 (2021-10-17)

### Bug Fixes

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** allow npm 8 14a4fea

## 3.0.0 (2021-09-23)

### ⚠ BREAKING CHANGES

- **deps:** update semantic-release monorepo

### Features

- **@html-validate/semantic-release-config:** include `semantic-release` as peer dependency 3208d3e

### Bug Fixes

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** require npm 7 8173455

### Dependency upgrades

- **deps:** update semantic-release monorepo 29436ac

### 2.1.2 (2021-09-18)

### Bug Fixes

- **@html-validate/semantic-release-config:** set channel as well 8e71481

### 2.1.1 (2021-09-18)

### Bug Fixes

- **@html-validate/semantic-release-config:** range must be set 0972add

## 2.1.0 (2021-09-18)

### Features

- **@html-validate/semantic-release-config:** maintenance releases from release/N.x a83a9e3

### 2.0.3 (2021-09-12)

### Dependency upgrades

- **deps:** update dependency @semantic-release/git to v9.0.1 fcd7d07
- **deps:** update dependency conventional-changelog-conventionalcommits to v4.6.1 5c44f07

### 2.0.2 (2021-08-08)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v6.2.2 4c15e0d

### 2.0.1 (2021-06-27)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v6.2.1 5f1a392
- **deps:** update dependency semantic-release-lerna to ^0.4.0 148eb6a

## 2.0.0 (2021-06-20)

### ⚠ BREAKING CHANGES

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** require NodeJS 12

### Features

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** require NodeJS 12 354cb9c

### Bug Fixes

- **@html-validate/semantic-release-monorepo-config:** semantic-release-monorepo-config requires node 12.10 0735535

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v6.2.0 54bbe21

### 1.2.15 (2021-06-13)

### Dependency upgrades

- **deps:** update dependency @semantic-release/release-notes-generator to v9.0.3 2b56113

### 1.2.14 (2021-06-06)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v6.1.0 290f464
- **deps:** update dependency semantic-release-lerna to ^0.3.0 4de2493

### 1.2.13 (2021-05-08)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v7.1.3 48851a3

### 1.2.12 (2021-05-02)

### Dependency upgrades

- **deps:** update dependency conventional-changelog-conventionalcommits to v4.6.0 a5b1204

### 1.2.11 (2021-04-11)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v7.1.1 f1ea3a5

### 1.2.10 (2021-04-04)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v7.1.0 939f414

### 1.2.9 (2021-03-14)

### Dependency upgrades

- **deps:** update dependency @semantic-release/release-notes-generator to v9.0.2 763ea6f

### 1.2.8 (2021-03-07)

### Dependency upgrades

- **deps:** update dependency semantic-release-lerna to ^0.2.0 abf6108

### [1.2.7](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.6...v1.2.7) (2021-02-28)

### Dependency upgrades

- **deps:** update dependency @html-validate/semantic-release-config to v1.2.6 ([708772b](https://gitlab.com/html-validate/semantic-release-config/commit/708772b0f6b05641c33c00ad5a4c54a4e587d289))

### [1.2.6](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.5...v1.2.6) (2021-01-31)

### Dependency upgrades

- **deps:** update dependency @semantic-release/gitlab to v6.0.9 ([1199792](https://gitlab.com/html-validate/semantic-release-config/commit/11997921a9769af8145f5bd12734b60d53dd2296))

### [1.2.5](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.4...v1.2.5) (2021-01-24)

### Dependency upgrades

- **deps:** update dependency @semantic-release/npm to v7.0.10 ([e3e5e81](https://gitlab.com/html-validate/semantic-release-config/commit/e3e5e8125b3f43a2df1413f22ea22e522e35ebfe))

### [1.2.4](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.3...v1.2.4) (2020-12-20)

### Dependency upgrades

- **deps:** update semantic-release monorepo ([4deff0f](https://gitlab.com/html-validate/semantic-release-config/commit/4deff0f267d52a5cc7caca81801e00c485ba03ea))

### [1.2.3](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.2...v1.2.3) (2020-12-16)

### Bug Fixes

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** add LICENSE files ([b04af39](https://gitlab.com/html-validate/semantic-release-config/commit/b04af392abd3685dcd3ab5b59d810d190086141e))

### [1.2.2](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.1...v1.2.2) (2020-12-16)

### Bug Fixes

- registry auth workaround ([53f0703](https://gitlab.com/html-validate/semantic-release-config/commit/53f07035656ace82a892a085ade775423349950c))

### [1.2.1](https://gitlab.com/html-validate/semantic-release-config/compare/v1.2.0...v1.2.1) (2020-12-16)

### Bug Fixes

- **@html-validate/semantic-release-monorepo-config:** commit assets ([a6f8119](https://gitlab.com/html-validate/semantic-release-config/commit/a6f811998565fcc6cb94eb6b1570dad9dd39adc7))

## [1.2.0](https://gitlab.com/html-validate/semantic-release-config/compare/v1.1.2...v1.2.0) (2020-12-16)

### Features

- **@html-validate/semantic-release-config, @html-validate/semantic-release-monorepo-config:** add monorepo configuration ([6c9666d](https://gitlab.com/html-validate/semantic-release-config/commit/6c9666d97151783171b236be5ad33a7ff40f0ab8))

# @html-validate/semantic-release-config changelog

### [1.1.2](https://gitlab.com/html-validate/semantic-release-config/compare/v1.1.1...v1.1.2) (2020-11-22)

### Dependency upgrades

- **deps:** update semantic-release monorepo ([6c5ad8c](https://gitlab.com/html-validate/semantic-release-config/commit/6c5ad8c83914b40bb96c620b9e7ace3ed99fa34e))

### [1.1.1](https://gitlab.com/html-validate/semantic-release-config/compare/v1.1.0...v1.1.1) (2020-11-15)

### Bug Fixes

- fix reference to `conventional-changelog-conventionalcommits` ([d24f7c4](https://gitlab.com/html-validate/semantic-release-config/commit/d24f7c4de5ebd2b3f8d911a1e668132b87559e62))

### Dependency upgrades

- **deps:** switch to upstream conventional-changelog-conventionalcommits ([51f7603](https://gitlab.com/html-validate/semantic-release-config/commit/51f7603e53320ff918a3f4f665284e77f52571c5))

## [1.1.0](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.44...v1.1.0) (2020-11-01)

### Features

- move `fix(deps)` to own section ([7ae5858](https://gitlab.com/html-validate/semantic-release-config/commit/7ae585819a8e36e661fd4b721cda2c4acac8ee40))

## [1.0.44](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.43...v1.0.44) (2020-11-01)

## [1.0.43](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.42...v1.0.43) (2020-10-25)

## [1.0.42](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.41...v1.0.42) (2020-10-18)

## [1.0.41](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.40...v1.0.41) (2020-10-11)

## [1.0.40](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.39...v1.0.40) (2020-10-04)

## [1.0.39](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.38...v1.0.39) (2020-09-27)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v6.0.5 ([3796727](https://gitlab.com/html-validate/semantic-release-config/commit/37967277043bbc09847b31c6d354bf41b83e9c90))

## [1.0.38](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.37...v1.0.38) (2020-09-20)

## [1.0.37](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.36...v1.0.37) (2020-09-13)

### Bug Fixes

- **deps:** update dependency @semantic-release/npm to v7.0.6 ([9c6c628](https://gitlab.com/html-validate/semantic-release-config/commit/9c6c628e77be26e230bf18e873e29dd457e40f7e))

## [1.0.36](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.35...v1.0.36) (2020-09-06)

## [1.0.35](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.34...v1.0.35) (2020-08-30)

## [1.0.34](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.33...v1.0.34) (2020-08-23)

### Bug Fixes

- **deps:** update dependency find-up to v5 ([f49a5df](https://gitlab.com/html-validate/semantic-release-config/commit/f49a5dfdf253c57a2840a4c96dc7c3d545afaf4a))

## [1.0.33](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.32...v1.0.33) (2020-08-16)

## [1.0.32](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.31...v1.0.32) (2020-08-09)

## [1.0.31](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.30...v1.0.31) (2020-08-02)

## [1.0.30](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.29...v1.0.30) (2020-07-26)

## [1.0.29](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.28...v1.0.29) (2020-07-19)

## [1.0.28](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.27...v1.0.28) (2020-07-12)

## [1.0.27](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.26...v1.0.27) (2020-07-05)

## [1.0.26](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.25...v1.0.26) (2020-06-28)

## [1.0.25](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.24...v1.0.25) (2020-06-21)

## [1.0.24](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.23...v1.0.24) (2020-06-14)

## [1.0.23](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.22...v1.0.23) (2020-06-07)

## [1.0.22](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.21...v1.0.22) (2020-05-31)

## [1.0.21](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.20...v1.0.21) (2020-05-17)

## [1.0.20](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.19...v1.0.20) (2020-05-10)

## [1.0.19](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.18...v1.0.19) (2020-05-03)

## [1.0.18](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.17...v1.0.18) (2020-04-26)

## [1.0.17](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.16...v1.0.17) (2020-04-19)

## [1.0.16](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.15...v1.0.16) (2020-04-12)

### Bug Fixes

- **deps:** update dependency @semantic-release/gitlab to v6.0.4 ([c86a60d](https://gitlab.com/html-validate/semantic-release-config/commit/c86a60da3c49d3515d8855e1963010b248362108))

## [1.0.15](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.14...v1.0.15) (2020-04-05)

## [1.0.14](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.13...v1.0.14) (2020-03-29)

### Bug Fixes

- **deps:** update semantic-release monorepo ([73a4180](https://gitlab.com/html-validate/semantic-release-config/commit/73a4180defd8a6fe303712eba2c7f256988e62e6))

## [1.0.13](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.12...v1.0.13) (2020-03-24)

### Bug Fixes

- support prettier 2 ([3d935d7](https://gitlab.com/html-validate/semantic-release-config/commit/3d935d70bd7de572819a2641840319e910321d18))

## [1.0.12](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.11...v1.0.12) (2020-03-22)

### Bug Fixes

- **deps:** update dependency @semantic-release/npm to v7.0.4 ([5956ad7](https://gitlab.com/html-validate/semantic-release-config/commit/5956ad707a3a6ad092262ea5f83cb6c10701bedb))
- **deps:** update dependency @semantic-release/npm to v7.0.5 ([613079d](https://gitlab.com/html-validate/semantic-release-config/commit/613079d5a3044080d99cdecc1ea97c30c68d1a93))

## [1.0.11](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.10...v1.0.11) (2020-03-01)

### Bug Fixes

- **deps:** update dependency @semantic-release/release-notes-generator to v9.0.1 ([ade316f](https://gitlab.com/html-validate/semantic-release-config/commit/ade316f565f5ddb54886a1ddf328f0a3991a5ea4))

## [1.0.10](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.9...v1.0.10) (2020-02-23)

## [1.0.9](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.8...v1.0.9) (2020-02-16)

### Bug Fixes

- **deps:** update semantic-release monorepo ([1f67096](https://gitlab.com/html-validate/semantic-release-config/commit/1f67096b77c2933f50cf08aac356ed30303ca248))

## [1.0.8](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.7...v1.0.8) (2020-02-02)

### Bug Fixes

- **deps:** update semantic-release monorepo ([762c9ef](https://gitlab.com/html-validate/semantic-release-config/commit/762c9efcc50a35329b13e7249bd8ff7f7e389690))
- **deps:** update semantic-release monorepo ([97d536a](https://gitlab.com/html-validate/semantic-release-config/commit/97d536a1bbe87b58bb26738ec32f6169574586c8))

## [1.0.7](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.6...v1.0.7) (2020-01-26)

## [1.0.6](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.5...v1.0.6) (2020-01-19)

### Bug Fixes

- **deps:** update semantic-release monorepo ([eccec9b](https://gitlab.com/html-validate/semantic-release-config/commit/eccec9b3cedfdadd2e2756a46e065c0489a76874))

## [1.0.5](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.4...v1.0.5) (2020-01-12)

## [1.0.4](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.3...v1.0.4) (2020-01-05)

## [1.0.3](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.2...v1.0.3) (2019-12-29)

### Bug Fixes

- **deps:** update dependency @semantic-release/npm to v5.3.5 ([e90aec6](https://gitlab.com/html-validate/semantic-release-config/commit/e90aec670ada17811a7cf7f7ef0a38df868a7076))

## [1.0.2](https://gitlab.com/html-validate/semantic-release-config/compare/v1.0.1...v1.0.2) (2019-12-24)

### Bug Fixes

- repo url ([ce12047](https://gitlab.com/html-validate/semantic-release-config/commit/ce120470ae2298a055163e19e1c1e5b640fd48fb))

## [1.0.1](https://gitlab.com/html-validate/semantic-release-html-validate/compare/v1.0.0...v1.0.1) (2019-12-24)

### Bug Fixes

- rename package ([2c78f10](https://gitlab.com/html-validate/semantic-release-html-validate/commit/2c78f10c0c2d648cc0be3a2a81246415b9ac8755))

# 1.0.0 (2019-12-24)

### Features

- initial release refactored from html-validate ([ff1b4b0](https://gitlab.com/html-validate/semantic-release-html-validate/commit/ff1b4b08b6527dced77e9c2bc7fec03f177bc411))
