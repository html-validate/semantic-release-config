# @html-validate/semantic-release-config

Semantic release configuration for the various HTML-validate packages.

- Uses `conventionalcommits` as base.
- Commits with `fix(deps)` is moved to a new section "Depenendency upgrades".
- Changelog with title from `package.json`, formatted with prettier.
- Publishes release to NPM and Gitlab and pushes commit to Git.

## Usage

Install:

```bash
npm install --save-dev semantic-release @html-validate/semantic-release-config
```

Configure:

```json
{
  "release": {
    "extends": "@html-validate/semantic-release-config"
  }
}
```

## Branches

### Regular releases

Released from `master`

### Maintenance releases

Released from `release/N.x` where `N` is major, e.g. `release/4.x`.

Minor releases can use `release/N.N.x`.

### Beta version

Released from `beta`
