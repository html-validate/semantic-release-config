const fs = require("node:fs");
const path = require("node:path");
const commitAnalyzer = require("@html-validate/semantic-release-config/lib/commit-analyzer");
const releaseNotesGenerator = require("@html-validate/semantic-release-config/lib/release-notes-generator");
const gitlab = require("@html-validate/semantic-release-config/lib/gitlab");
const changelog = require("@html-validate/semantic-release-config/lib/changelog");
const exec = require("@html-validate/semantic-release-config/lib/exec");
const git = require("@html-validate/semantic-release-config/lib/git");
const branches = require("@html-validate/semantic-release-config/lib/branches");

/**
 * @param {string} name
 * @param {string} cwd
 * @returns {string | undefined}
 */
function findUp(name, cwd) {
	let directory = path.resolve(cwd);
	const { root } = path.parse(directory);
	const stopAt = path.resolve(directory, root);

	while (directory !== stopAt) {
		const filePath = path.resolve(directory, name);
		if (fs.existsSync(filePath) && fs.statSync(filePath).isFile()) {
			return filePath;
		}

		directory = path.dirname(directory);
	}

	return undefined;
}

const PACKAGE_JSON = "package.json";
const pkgPath = findUp(PACKAGE_JSON, process.cwd());
const { workspaces } = JSON.parse(fs.readFileSync(pkgPath, "utf-8"));

const pkgJson = workspaces.map((it) => path.join(it, PACKAGE_JSON));

const plugins = [
	["@semantic-release/commit-analyzer", commitAnalyzer],
	[
		"semantic-release-lerna",
		{
			...releaseNotesGenerator,
			generateNotes: true,
		},
	],
	["@semantic-release/gitlab", gitlab],
	["@semantic-release/changelog", changelog],
	["@semantic-release/exec", exec],
	[
		"@semantic-release/git",
		{
			...git,
			assets: ["CHANGELOG.md", "package.json", "package-lock.json", ...pkgJson],
		},
	],
];

module.exports = { branches, plugins };
