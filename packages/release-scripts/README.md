# @html-validate/release-scripts

Various scripts used by release toolchain.

## `release-pre{pack,publish}` and `release-post{pack,publish}`

`package.json`:

```json
{
  "scripts": {
    "prepack": "release-prepack",
    "postpack": "release-postpack",
    "prepublishOnly": "release-prepublish",
    "postpublish": "release-postpublish"
  }
}
```

If the library or CLI tool bundles all dependencies use `release-prepack --bundle`.
Dependencies can also be filtered by marking dependencies as external using `--bundle --external:foo --external:bar` or listing them in the non-standard `externalDependencies` field `"externalDependencies": ["foo", "bar"]`.

```json
{
  "scripts": {
    "prepack": "release-prepack --bundle --external:foo --external:bar",
    "postpack": "release-postpack",
    "prepublishOnly": "release-prepublish --bundle --external:foo --external:bar",
    "postpublish": "release-postpublish"
  }
}
```

or

```json
{
  "scripts": {
    "prepack": "release-prepack --bundle",
    "postpack": "release-postpack",
    "prepublishOnly": "release-prepublish --bundle",
    "postpublish": "release-postpublish"
  },
  "externalDependencies": ["foo", "bar"]
}
```

### Usage

    release-prepack [FILENAME] [OPTIONS..]
    release-postpack [FILENAME]
    release-prepublish [FILENAME] [OPTIONS..]
    release-postpublish [FILENAME]

If `FILENAME` is set it is the filename to use instead of the default `package.json`.

### Options

#### `--bundle`

By default the `dependencies` field in `package.json` is intact but if your package bundles its dependencies the `--bundle` flag can be used to strip out dependencies as well.

#### `--external:${NAME}`

When using in conjunction with `--bundle` it marks a dependency which should still be intact.
For instance, using `--external:foobar` all packages in `dependencies` except `foobar` will be stripped.
This flag can be used multiple times.

#### `--retain-scripts`

By default the `scripts` field is stripped but with this flag enabled the scripts will be intact.
