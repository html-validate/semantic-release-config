/**
 * @param {string[]} argv
 * @returns {[flags: string[], positionals: string[]]}
 */
function getArgs(argv) {
	const flags = argv.filter((it) => it.startsWith("--"));
	const positionals = argv.filter((it) => !it.startsWith("--"));
	return [flags, positionals];
}

module.exports = { getArgs };
