const { stripFields } = require("./strip-fields");

it("should strip devDependencies", () => {
	expect.assertions(1);
	const pkg = {
		name: "mock",
		devDependencies: {
			foo: "^1.0.0",
		},
	};
	expect(stripFields(pkg)).toEqual({
		name: "mock",
	});
});

it("should strip scripts by default", () => {
	expect.assertions(1);
	const pkg = {
		name: "mock",
		scripts: {
			test: "true",
		},
	};
	expect(stripFields(pkg)).toEqual({
		name: "mock",
	});
});

it("should retain scripts if retainScript is used", () => {
	expect.assertions(1);
	const pkg = {
		name: "mock",
		scripts: {
			test: "true",
		},
	};
	expect(stripFields(pkg, { retainScripts: true })).toEqual({
		name: "mock",
		scripts: {
			test: "true",
		},
	});
});

describe("should strip", () => {
	it.each([
		"ava",
		"c8",
		"commitlint",
		"greenkeeper",
		"husky",
		"jest",
		"lint-staged",
		"nyc",
		"pre-commit",
		"prettier",
		"release",
		"renovate",
		"simple-git-hooks",
		"stylelint",
		"tsd",
		"xo",
	])("%s", (field) => {
		expect.assertions(1);
		const pkg = {
			name: "mock",
			[field]: "anything",
		};
		expect(stripFields(pkg)).toEqual({
			name: "mock",
		});
	});
});
