const { getArgs } = require("./get-args");

it("should separate flags and positionals from argv", () => {
	expect.assertions(2);
	const argv = ["spam", "--foo", "--bar", "baz"];
	const [flags, positionals] = getArgs(argv);
	expect(flags).toEqual(["--foo", "--bar"]);
	expect(positionals).toEqual(["spam", "baz"]);
});
