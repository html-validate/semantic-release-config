const fs = require("fs");
const { readPackageJson } = require("./read-package-json");

const mockPackage = {
	name: "mock-package",
	version: "1.2.3",
};

afterEach(() => {
	jest.restoreAllMocks();
});

it("should read and parse package.json", () => {
	expect.assertions(2);
	const readFileSync = jest.spyOn(fs, "readFileSync").mockReturnValue(JSON.stringify(mockPackage));
	const pkg = readPackageJson("./package.json");
	expect(pkg).toEqual(mockPackage);
	expect(readFileSync).toHaveBeenCalledWith("./package.json", "utf-8");
});
