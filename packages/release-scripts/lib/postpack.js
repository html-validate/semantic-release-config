/* eslint-disable no-console -- cli script, expected to print to console */
/* eslint-disable n/no-process-exit -- cli script, want it to exit with non-zero status */

const path = require("path");
const fs = require("fs");

/* ensure this script runs via the correct lifecycle event */
const { npm_command: command, npm_lifecycle_event: event } = process.env;
if (command === "publish" && event === "postpack") {
	process.exit(0);
}

const filename = process.argv[2] || "package.json";
const filepath = path.resolve(filename);
const origfile = `${filepath}.orig`;

console.log(`Restoring "${filepath}"`);
fs.renameSync(origfile, filepath);
