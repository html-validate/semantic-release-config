/* eslint-disable no-console -- cli script, expected to print to console */
/* eslint-disable n/no-process-exit -- cli script, want it to exit with non-zero status */

const path = require("path");
const fs = require("fs");
const { filterDependencies } = require("./filter-dependencies");
const { getArgs } = require("./get-args");
const { readPackageJson } = require("./read-package-json");
const { stripFields } = require("./strip-fields");

/* ensure this script runs via the correct lifecycle event */
const { npm_command: command, npm_lifecycle_event: event } = process.env;
if (command === "publish" && event === "prepack") {
	process.exit(0);
}

const [flags, positionals] = getArgs(process.argv.slice(2));
const bundle = flags.includes("--bundle");
const retainScripts = flags.includes("--retain-scripts");
const externalArgs = flags
	.filter((it) => it.startsWith("--external:"))
	.map((it) => it.slice("--external:".length));

const filename = positionals[0] || "package.json";
const filePath = path.resolve(filename);
const tmpfile = `${filePath}.tmp`;
const origfile = `${filePath}.orig`;

if (!fs.existsSync(filePath)) {
	process.stderr.write(`release-prepack: No such file or directory: "${filePath}"\n`);
	process.exit(1);
}

const pkg = readPackageJson(filePath);
const externalField = pkg.externalDependencies || [];
pkg.dependencies = filterDependencies(pkg.dependencies, bundle, [
	...externalArgs,
	...externalField,
]);

const stripped = stripFields(pkg, {
	retainScripts,
});

const content = JSON.stringify(stripped, null, 2);
console.log(content);

/* backup original file in case of error */
fs.copyFileSync(filePath, origfile);

try {
	/* write to a temporary file first */
	fs.writeFileSync(tmpfile, `${content}\n`, "utf-8");

	/* swap tempfile for original */
	fs.renameSync(tmpfile, filePath);
} catch (err) {
	console.error(err);
	process.stderr.write(`\nRestoring "${filename}" after failure\n`);
	fs.copyFileSync(origfile, filePath);
	process.exit(1);
}
