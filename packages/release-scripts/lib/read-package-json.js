const fs = require("fs");

/**
 * @param {string} filePath
 * @returns {object}
 */
function readPackageJson(filePath) {
	return JSON.parse(fs.readFileSync(filePath, "utf-8"));
}

module.exports = { readPackageJson };
