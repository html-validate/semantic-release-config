const { filterDependencies } = require("./filter-dependencies");

const dependencies = {
	foo: "1",
	bar: "2",
	baz: "3",
};

it("should return original when not bundling", () => {
	expect.assertions(1);
	expect(filterDependencies(dependencies, false, [])).toEqual({
		foo: "1",
		bar: "2",
		baz: "3",
	});
});

it("should return undefined when bundling", () => {
	expect.assertions(1);
	expect(filterDependencies(dependencies, true, [])).toBeUndefined();
});

it("should return filtered list when using externals", () => {
	expect.assertions(1);
	expect(filterDependencies(dependencies, true, ["bar"])).toEqual({
		bar: "2",
	});
});

it("should return undefined when externals does not match anything", () => {
	expect.assertions(1);
	expect(filterDependencies(dependencies, true, ["spam"])).toBeUndefined();
});
