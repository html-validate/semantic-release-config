/**
 * @typedef {object} StripOptions
 * @property {boolean} [retainScripts]
 */

const strip = [
	"ava",
	"c8",
	"commitlint",
	"devDependencies",
	"externalDependencies",
	"greenkeeper",
	"husky",
	"jest",
	"lint-staged",
	"nyc",
	"overrides",
	"pre-commit",
	"prettier",
	"release",
	"renovate",
	"simple-git-hooks",
	"stylelint",
	"tsd",
	"volta",
	"workspaces",
	"xo",
];

/**
 * @template T
 * @param {T} pkg
 * @param {StripOptions} options
 * @returns {Partial<T>}
 */
function stripFields(pkg, options = {}) {
	const result = { ...pkg };
	for (const key of strip) {
		delete result[key];
	}

	if (!options.retainScripts) {
		delete result.scripts;
	}

	return result;
}

module.exports = { stripFields };
