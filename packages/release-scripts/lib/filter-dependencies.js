/**
 * @param {Record<string, string>} dependencies
 * @param {boolean} bundle
 * @param {string[]} externals
 * @returns {Record<string, string> | undefined}
 */
function filterDependencies(dependencies, bundle, externals) {
	if (!bundle) {
		return dependencies;
	}
	if (externals.length === 0) {
		return undefined;
	}
	const filtered = Object.entries(dependencies).filter(([key]) => {
		return externals.includes(key);
	});
	if (filtered.length === 0) {
		return undefined;
	}
	return Object.fromEntries(filtered);
}

module.exports = { filterDependencies };
