/* eslint-disable camelcase -- npm environment variables are in camelcase */

import { existsSync, realpathSync } from "node:fs";
import fs from "node:fs/promises";
import { randomBytes } from "node:crypto";
import os from "node:os";
import path from "node:path";
import spawn from "nano-spawn";

const tempdir = realpathSync(os.tmpdir());

async function withTempDir(callback: (testPath: string) => Promise<void>): Promise<void> {
	const testPath = path.join(tempdir, randomBytes(16).toString("hex"));
	await fs.mkdir(testPath);
	try {
		await callback(testPath);
	} finally {
		await fs.rm(testPath, { recursive: true, force: true, maxRetries: 2 });
	}
}

async function prepublish(command: string, ...args: string[]): Promise<void> {
	const binary = path.join(__dirname, "../scripts/prepublish");
	await spawn(binary, args, {
		env: {
			npm_command: command,
			npm_lifecycle_event: "prepublish",
		},
	});
}

async function prepack(command: string, ...args: string[]): Promise<void> {
	const binary = path.join(__dirname, "../scripts/prepack");
	await spawn(binary, args, {
		env: {
			npm_command: command,
			npm_lifecycle_event: "prepack",
		},
	});
}

async function postpack(command: string, ...args: string[]): Promise<void> {
	const binary = path.join(__dirname, "../scripts/postpack");
	await spawn(binary, args, {
		env: {
			npm_command: command,
			npm_lifecycle_event: "postpack",
		},
	});
}

async function postpublish(command: string, ...args: string[]): Promise<void> {
	const binary = path.join(__dirname, "../scripts/postpublish");
	await spawn(binary, args, {
		env: {
			npm_command: command,
			npm_lifecycle_event: "postpublish",
		},
	});
}

async function prepareTest(
	testPath: string,
	filename: string = "example.json",
): Promise<{ packageJson: string; tempFile: string; origFile: string }> {
	const packageJson = path.join(testPath, "package.json");
	const tempFile = path.join(testPath, "package.json.tmp");
	const origFile = path.join(testPath, "package.json.orig");
	await fs.copyFile(path.join(__dirname, filename), packageJson);
	return { packageJson, tempFile, origFile };
}

describe("prepack", () => {
	it("should remove unnecessary fields from package.json", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", packageJson);
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should remove dependencies from package.json when --bundle is used", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", packageJson, "--bundle");
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should filter dependencies from package.json when --external is used", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", packageJson, "--bundle", "--external:runtime-dependency");
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should filter dependencies from the externalDependencies field", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath, "external.json");
			await prepack("pack", packageJson, "--bundle");
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should support arguments in any order", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", "--external:runtime-dependency", "--bundle", packageJson);
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should retain scripts if --retain-scripts is used", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", packageJson, "--retain-scripts");
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeTruthy();
		});
	});

	it("should return non-zero on failure", () => {
		expect.assertions(1);
		return withTempDir(async (testPath) => {
			await prepareTest(testPath);
			await expect(prepack("pack", "invalid.json")).rejects.toEqual(
				expect.objectContaining({
					exitCode: 1,
					stderr: expect.stringContaining("No such file or directory"),
				}),
			);
		});
	});

	it("should keep original file as is in case of errors", () => {
		expect.assertions(4);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await expect(prepack("pack", "invalid.json")).rejects.toBeDefined();
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeFalsy();
		});
	});

	it("should not do anything when originating from publish command", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("publish", packageJson);
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeFalsy();
		});
	});
});

describe("postpack", () => {
	it("should restore package.json", () => {
		expect.assertions(3);
		return withTempDir(async (testPath) => {
			const { packageJson, tempFile, origFile } = await prepareTest(testPath);
			await prepack("pack", packageJson);
			await postpack("pack", packageJson);
			const result = await fs.readFile(packageJson, "utf-8");
			expect(result).toMatchSnapshot();
			expect(existsSync(tempFile)).toBeFalsy();
			expect(existsSync(origFile)).toBeFalsy();
		});
	});
});

it("npm pack integration test", () => {
	expect.assertions(2);
	return withTempDir(async (testPath) => {
		const { packageJson } = await prepareTest(testPath);
		await prepack("pack", packageJson, "--bundle");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpack("pack", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
	});
});

it("npm pack integration test (--retain-scripts)", () => {
	expect.assertions(2);
	return withTempDir(async (testPath) => {
		const { packageJson } = await prepareTest(testPath);
		await prepack("pack", packageJson, "--bundle", "--retain-scripts");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpack("pack", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
	});
});

it("npm publish integration test", () => {
	expect.assertions(4);
	return withTempDir(async (testPath) => {
		const { packageJson } = await prepareTest(testPath);
		await prepublish("publish", packageJson, "--bundle");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await prepack("publish", packageJson, "--bundle");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpack("publish", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpublish("publish", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
	});
});

it("npm publish integration test (--retain-scripts)", () => {
	expect.assertions(4);
	return withTempDir(async (testPath) => {
		const { packageJson } = await prepareTest(testPath);
		await prepublish("publish", packageJson, "--bundle", "--retain-scripts");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await prepack("publish", packageJson, "--bundle", "--retain-scripts");
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpack("publish", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
		await postpublish("publish", packageJson);
		expect(await fs.readFile(packageJson, "utf-8")).toMatchSnapshot();
	});
});
