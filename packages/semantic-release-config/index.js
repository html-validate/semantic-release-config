const commitAnalyzer = require("./lib/commit-analyzer");
const releaseNotesGenerator = require("./lib/release-notes-generator");
const npm = require("./lib/npm");
const gitlab = require("./lib/gitlab");
const changelog = require("./lib/changelog");
const exec = require("./lib/exec");
const git = require("./lib/git");
const branches = require("./lib/branches");

const plugins = [
	["@semantic-release/commit-analyzer", commitAnalyzer],
	["@semantic-release/release-notes-generator", releaseNotesGenerator],
	["@semantic-release/npm", npm],
	["@semantic-release/gitlab", gitlab],
	["@semantic-release/changelog", changelog],
	["@semantic-release/exec", exec],
	["@semantic-release/git", git],
];

module.exports = { branches, plugins };
