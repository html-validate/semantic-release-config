module.exports = {
	message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}",
};
