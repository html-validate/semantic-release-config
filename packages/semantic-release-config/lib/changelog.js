const fs = require("node:fs");
const path = require("node:path");

/**
 * @param {string} name
 * @param {string} cwd
 * @returns {string | undefined}
 */
function findUp(name, cwd) {
	let directory = path.resolve(cwd);
	const { root } = path.parse(directory);
	const stopAt = path.resolve(directory, root);

	while (directory !== stopAt) {
		const filePath = path.resolve(directory, name);
		if (fs.existsSync(filePath) && fs.statSync(filePath).isFile()) {
			return filePath;
		}

		directory = path.dirname(directory);
	}

	return undefined;
}

const pkgPath = findUp("package.json", process.cwd());
const pkg = JSON.parse(fs.readFileSync(pkgPath, "utf-8"));

module.exports = {
	changelogTitle: `# ${pkg.name} changelog`,
};
