const _ = require("lodash");
const micromatch = require("micromatch");
const branches = require("./branches");

const patterns = branches.map((it) => {
	if (typeof it === "string") {
		return it;
	} else {
		return it.name;
	}
});

const positive = ["master", "beta", "release/4.x", "release/13.x", "release/2.3.x"];
const negative = ["foo", "feature/foo", "feature/master", "feature/4.x", "renovate/dependency-3.x"];

describe("should match", () => {
	it.each(positive)("%s", (branch) => {
		expect.assertions(1);
		expect(micromatch.isMatch(branch, patterns)).toBeTruthy();
	});
});

describe("should not match", () => {
	it.each(negative)("%s", (branch) => {
		expect.assertions(1);
		expect(micromatch.isMatch(branch, patterns)).toBeFalsy();
	});
});

it("maintenance branch should set proper range and channel", () => {
	expect.assertions(2);
	const config = branches.find((it) => it.name && it.name.startsWith("release/"));
	expect(_.template(config.range)({ name: "release/4.x" })).toBe("4.x");
	expect(_.template(config.channel)({ name: "release/4.x" })).toBe("4.x");
});
